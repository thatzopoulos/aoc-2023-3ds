//! RomFS example.
//!
//! This example showcases the RomFS service and how to mount it to include a read-only filesystem within the application bundle.
mod day01;
mod day02;
mod day03;
mod day04;
use ctru::prelude::*;

fn main() {
    let gfx = Gfx::new().expect("Couldn't obtain GFX controller");
    let mut hid = Hid::new().expect("Couldn't obtain HID controller");
    let apt = Apt::new().expect("Couldn't obtain APT controller");
    // let _console = Console::new(gfx.top_screen.borrow_mut());
    // Start a console on the top screen
    let top_screen = Console::new(gfx.top_screen.borrow_mut());

    // Start a console on the bottom screen.
    // The most recently initialized console will be active by default.
    let bottom_screen = Console::new(gfx.bottom_screen.borrow_mut());

    top_screen.select();
    cfg_if::cfg_if! {
        // Run this code if RomFS are wanted and available.
        // This never fails as `ctru-rs` examples inherit all of the `ctru-rs` features,
        // but it might if a normal user application wasn't setup correctly.
        if #[cfg(all(feature = "romfs", romfs_exists))] {
            // Mount the romfs volume.
            let _romfs = ctru::services::romfs::RomFS::new().unwrap();

            top_screen.select();
            let result = day01::part1();
            println!("Day 1 Part 1: {result}");
            let result2 = day01::part2();
            println!("Day 1 Part 2: {result2}");

            let result3 = day02::part1();
            println!("Day 2 Part 1: {result3}");
            let result4 = day02::part2();
            println!("Day 2 Part 2: {result4}");

            let result5 = day03::part1();
            println!("Day 3 Part 1: {result5}");
            let result6 = day03::part2();
            println!("Day 3 Part 2: {result6}");

            let result7 = day04::part1();
            println!("Day 4 Part 1: {result7}");
            let result8 = day04::part2();
            println!("Day 4 Part 2: {result8}");

        } else {
            println!("No RomFS was found, are you sure you included it?")
        }
    }
    top_screen.select();
    println!("\x1b[29;16HPress Start to exit");

    while apt.main_loop() {
        hid.scan_input();

        if hid.keys_down().contains(KeyPad::START) {
            break;
        }

        gfx.wait_for_vblank();
    }
}
