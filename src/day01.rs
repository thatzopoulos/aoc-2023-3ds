pub fn part1() -> u32 {
    // Open a simple text file present in the RomFS volume.
    // Remember to use the `romfs:/` prefix when working with `RomFS`.
    let f = std::fs::read_to_string("romfs:/day01.txt").unwrap();

    let lines: Vec<String> = f.lines().map(String::from).collect();

    let mut total_sum = 0;

    for line in lines {
        // Find the first digit and the last digit in the line
        if let Some(first_digit) = line.chars().find(|c| c.is_ascii_digit()) {
            if let Some(last_digit) = line.chars().rev().find(|c| c.is_ascii_digit()) {
                // Combine into a two-digit number
                let value =
                    first_digit.to_digit(10).unwrap() * 10 + last_digit.to_digit(10).unwrap();

                total_sum += value;
            }
        }
    }
    total_sum
}

pub fn part2() -> u32 {
    // Open a simple text file present in the RomFS volume.
    // Remember to use the `romfs:/` prefix when working with `RomFS`.
    let f = std::fs::read_to_string("romfs:/day01.txt").unwrap();

    let lines: Vec<String> = f.lines().map(String::from).collect();

    let mut total_sum = 0;

    for mut line in lines {
        // Replace instances of spelled out digits
        line = line
            .replace("one", "o1e")
            .replace("two", "t2o")
            .replace("three", "thre3e")
            .replace("four", "f4ur")
            .replace("five", "fi5e")
            .replace("six", "s6x")
            .replace("seven", "se7en")
            .replace("eight", "ei8ht")
            .replace("nine", "n9ne");

        // Find the first digit and the last digit in the line
        if let Some(first_digit) = line.chars().find(|c| c.is_ascii_digit()) {
            if let Some(last_digit) = line.chars().rev().find(|c| c.is_ascii_digit()) {
                // Combine into a two-digit number
                let value =
                    first_digit.to_digit(10).unwrap() * 10 + last_digit.to_digit(10).unwrap();

                total_sum += value;
            }
        }
    }
    total_sum
}
