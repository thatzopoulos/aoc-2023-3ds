use std::{cmp::max, collections::HashMap};
pub fn part1() -> i32 {
    let color_counts: HashMap<&str, i32> =
        HashMap::from([("red", 12), ("green", 13), ("blue", 14)]);
    let mut total_sum = 0;
    let f = std::fs::read_to_string("romfs:/day02.txt").unwrap();
    let lines: Vec<String> = f.lines().map(String::from).collect();

    for line in lines {
        let parts: Vec<&str> = line.split(": ").collect();
        let game_id: i32 = parts[0][5..].parse().unwrap();
        let sets: Vec<&str> = parts[1].split("; ").collect();

        let mut valid = true;
        for set in sets {
            let mut cubes = HashMap::new();
            for cube in set.split(", ") {
                let cube_parts: Vec<&str> = cube.split(" ").collect();
                let count: i32 = cube_parts[0].parse().unwrap();
                let color = cube_parts[1];

                let current_count = cubes.entry(color).or_insert(0);
                *current_count += count;

                if let Some(&count) = color_counts.get(color) {
                    if *current_count > count {
                        valid = false;
                        break;
                    }
                }
            }
        }

        if valid {
            total_sum += game_id;
        }
    }

    total_sum
}

pub fn part2() -> i32 {
    let mut total_sum = 0;
    let f = std::fs::read_to_string("romfs:/day02.txt").unwrap();
    let lines: Vec<String> = f.lines().map(String::from).collect();
    for line in lines {
        let parts: Vec<&str> = line.split(": ").collect();
        let sets: Vec<&str> = parts[1].split("; ").collect();

        let mut cubes: HashMap<&str, i32> = HashMap::new();

        for set in sets {
            for cube in set.split(", ") {
                let cube_parts: Vec<&str> = cube.split(" ").collect();
                let count: i32 = cube_parts[0].parse().unwrap();
                let color = cube_parts[1];

                let current_count = cubes.entry(color).or_insert(0);
                *current_count = max(count, *current_count);
            }
        }

        total_sum += cubes.values().fold(1, |acc, &val| acc * val);
    }

    total_sum
}
