//
pub fn part1() -> i32 {
    let mut result = 0;
    let f = std::fs::read_to_string("romfs:/day03.txt").unwrap();

    let lines: Vec<String> = f.lines().map(String::from).collect();

    let mut numbers = Vec::new();

    for (i, row) in lines.iter().enumerate() {
        let mut j = 0;
        while j < row.len() {
            if row.chars().nth(j).unwrap().is_numeric() {
                let mut num = String::new();
                let start_j = j;
                while j < row.len() && row.chars().nth(j).unwrap().is_numeric() {
                    num.push(row.chars().nth(j).unwrap());
                    j += 1;
                }
                numbers.push(((i, start_j), num));
            } else {
                j += 1;
            }
        }
    }

    for ((x, y), num) in &numbers {
        if has_symbol_neighbor(lines.clone(), *x, *y, num.len()) {
            result += num.parse::<i32>().unwrap();
        }
    }

    result
}

fn has_symbol_neighbor(grid: Vec<String>, x: usize, y: usize, length: usize) -> bool {
    let dx = [-1, -1, -1, 0, 0, 1, 1, 1];
    let dy = [-1, 0, 1, -1, 1, -1, 0, 1];

    for offset in 0..length {
        let y_offset = y + offset;
        for k in 0..8 {
            let nx = x as i32 + dx[k];
            let ny = y_offset as i32 + dy[k];

            if nx >= 0 && ny >= 0 && nx < grid.len() as i32 && ny < grid[0].len() as i32 {
                let neighbor = grid[nx as usize].chars().nth(ny as usize).unwrap();
                if !neighbor.is_numeric() && neighbor != '.' {
                    // debug!("Neighbor at ({}, {}) is symbol: {}", nx, ny, neighbor);
                    return true;
                }
            }
        }
    }
    false
}

//
//
pub fn part2() -> i32 {
    let f = std::fs::read_to_string("romfs:/day03.txt").unwrap();

    let lines: Vec<String> = f.lines().map(String::from).collect();
    let mut total_gear_ratio = 0;
    for (i, row) in lines.iter().enumerate() {
        for (j, cell) in row.chars().enumerate() {
            if cell == '*' {
                if let Some(gear_ratio) = calc_gear_ratio(&lines, i, j) {
                    total_gear_ratio += gear_ratio;
                }
            }
        }
    }

    total_gear_ratio
}

fn calc_gear_ratio(grid: &Vec<String>, x: usize, y: usize) -> Option<i32> {
    let mut adjacent_numbers = Vec::new();

    // Directions: up, down, left, right, and diagonals
    let directions = [
        (-1, 0),
        (1, 0),
        (0, -1),
        (0, 1),
        (-1, -1),
        (-1, 1),
        (1, -1),
        (1, 1),
    ];
    for (dx, dy) in directions.iter() {
        let nx = x as i32 + dx;
        let ny = y as i32 + dy;
        if nx >= 0 && ny >= 0 && nx < grid.len() as i32 && ny < grid[0].len() as i32 {
            let neighbor = grid[nx as usize].chars().nth(ny as usize);
            if neighbor.is_some() && neighbor.unwrap().is_numeric() {
                let number = extract_number(grid, nx as usize, ny as usize);
                if !adjacent_numbers.contains(&number) {
                    adjacent_numbers.push(number);
                }
            }
        }
    }

    // Remove duplicates
    adjacent_numbers.sort_unstable();
    adjacent_numbers.dedup();

    // Calculate gear ratio if there are exactly two unique adjacent numbers
    if adjacent_numbers.len() == 2 {
        Some(adjacent_numbers[0] * adjacent_numbers[1])
    } else {
        None
    }
}

fn extract_number(grid: &Vec<String>, x: usize, y: usize) -> i32 {
    let row = &grid[x];
    let mut num_str = String::new();

    // Extract to the left
    let mut j = y;
    while j > 0 && row.chars().nth(j - 1).unwrap().is_numeric() {
        j -= 1; // Move to the start of the number
    }

    // Extract the whole number
    while j < row.len() && row.chars().nth(j).unwrap().is_numeric() {
        num_str.push(row.chars().nth(j).unwrap());
        j += 1;
    }

    num_str.parse::<i32>().unwrap_or(0)
}
