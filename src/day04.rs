use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Debug)]
struct Card {
    card_number: i32,
    winning_numbers: Vec<i32>,
    players_numbers: Vec<i32>,
}

fn read_cards_from_file() -> io::Result<Vec<Card>> {
    let mut cards = Vec::new();
    let f = std::fs::read_to_string("romfs:/day04.txt").unwrap();

    let lines: Vec<String> = f.lines().map(String::from).collect();

    for line in lines {
        // Split the line at ':'
        let parts: Vec<&str> = line.splitn(2, ':').collect();
        if parts.len() != 2 {
            continue; // Skip lines that don't have the correct format
        }

        // Extract the card number
        let card_number_str = parts[0].trim_start_matches("Card ");
        let card_number = card_number_str.parse::<i32>().unwrap_or(0);

        // Split the remaining part at '|'
        let numbers_parts: Vec<&str> = parts[1].split('|').collect();
        if numbers_parts.len() != 2 {
            continue; // Skip lines that don't have the correct format
        }

        let winning_numbers = numbers_parts[0]
            .split_whitespace()
            .filter_map(|s| s.parse::<i32>().ok())
            .collect();

        let players_numbers = numbers_parts[1]
            .split_whitespace()
            .filter_map(|s| s.parse::<i32>().ok())
            .collect();

        cards.push(Card {
            card_number,
            winning_numbers,
            players_numbers,
        });
    }

    Ok(cards)
}

//
pub fn part1() -> i32 {
    let mut total = 0;
    // let lines: Vec<String> = include_str!("../input/day04.txt")
    //     .lines();
    match read_cards_from_file() {
        Ok(cards) => {
            for card in cards {
                let card_wins = card
                    .winning_numbers
                    .iter()
                    .filter(|&x| card.players_numbers.contains(x))
                    .count();
                match card_wins {
                    1 => total += 1,
                    0 => (),
                    x => {
                        let points = 1 * 2_i32.pow((x - 1).try_into().unwrap());
                        total += points
                    }
                }
            }
        }
        Err(e) => println!("Error reading file: {}", e),
    }

    total
}

//
//
pub fn part2() -> usize {
    let mut total = 0;
    match read_cards_from_file() {
        Ok(cards) => {
            let final_card_counts = process_cards(&cards);
            total = final_card_counts.iter().sum();
        }
        Err(e) => println!("Error reading file: {}", e),
    }

    total
}

fn count_matches(card: &Card) -> usize {
    card.winning_numbers
        .iter()
        .filter(|&&num| card.players_numbers.contains(&num))
        .count()
}

fn process_cards(cards: &Vec<Card>) -> Vec<usize> {
    let mut total_cards = vec![0; cards.len()];

    for (i, card) in cards.iter().enumerate() {
        let matches = count_matches(card);
        total_cards[i] += 1;

        let copies = total_cards[i];
        for j in 1..=matches {
            if i + j < total_cards.len() {
                total_cards[i + j] += copies;
            }
        }
    }

    total_cards
}
